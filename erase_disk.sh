#!/bin/bash
#
# Dies ist das Shellskript erase_disk.sh. Es dient im Rahmen des Projektes einer Loeschstation
# zum Analysieren, Löschen und Reporting von Festplaten und SSDs. Es wird durch das Ausfuehren
# des Shellskripts detect_disk.sh mit einem Uebergabeparameter eines aktivierten Laufwerkes
# initiiert.


# Deklaration der Geraetekennung aus dem Uebergabeparameter vom Skript detect_disk.sh
d=$1

Report() {
	# Statusausgabe: Beginn Protokollerstellung.
	DT=$(date +%Y-%m-%d_%H:%M:%S)
	echo "$DT /dev/$d: Start protocol" >> /dev/tty1
	echo "$DT /dev/$d: Start protocol" >> /var/log/Erase-Station/log
	# Deklaration Variable fuer Zeitstempel: Jahr-Monat-Tag_Stunde:Minute:Sekunde.
	DT=$(date +%Y-%m-%d_%H:%M:%S)
	# Deklaration Variable fuer Dateiname des Pruefberichtes.
	Dateiname="HDD-Report_$DT.log"
	# Deklaration Variable der Seriennummer des Mediums.
	Seriennummer=$(hdparm -I /dev/$d | grep -i "serial number")
	# Deklaration Variable des SMART-Attributes der Laufleistung in Stunden.
	SMART9=$(smartctl -a /dev/$d | grep -i "Power_On_Hours")
	# Deklaration Variable mit extrahierten, numerischen Wert in Stunden.
	hour=$(echo $SMART9 | cut -d" " -f10)
	# Erstellung der Datei des Pruefberichtes mit der Variable dateiname.
	touch /var/log/Erase-Station/$Dateiname
	# Auslesen der ersten 16 Zeilen mit Stammdaten des Mediums und Speicherung in die Log-Datei.
	smartctl -a /dev/$d | head -n 16 /var/log/Erase-Station/$Dateiname
	# Einfuegen einer Leerzeile in die Log-Datei.
	echo >> /var/log/Erase-Station/$Dateiname
	# Abfragen der ausfallrelevanten SMART-Attribute.
	smartctl -a /dev/$d | grep -i "SMART Attributes Data Structure revision number" >> /var/log/Erase-Station/$Dateiname
	smartctl -a /dev/$d | grep -i "Vendor Specific SMART Attributes" >> /var/log/Erase-Station/$Dateiname
	smartctl -a /dev/$d | grep -i "ID# ATTRIBUTE_NAME" >> /var/log/Erase-Station/$Dateiname
	smartctl -a /dev/$d | grep -i "Raw_Read_Error" >> /var/log/Erase-Station/$Dateiname				# S.M.A.R.T.-ID   1
	smartctl -a /dev/$d | grep -i "Reallocated_Sector_Ct" >> /var/log/Erase-Station/$Dateiname		# S.M.A.R.T.-ID   5
	smartctl -a /dev/$d | grep -i "Power_On_Hours" >> /var/log/Erase-Station/$Dateiname				# S.M.A.R.T.-ID   9
	smartctl -a /dev/$d | grep -i "Spin_Retry_Count" >> /var/log/Erase-Station/$Dateiname			# S.M.A.R.T.-ID  10
	smartctl -a /dev/$d | grep -i "End-to-End_Error" >> /var/log/Erase-Station/$Dateiname			# S.M.A.R.T.-ID 184
	smartctl -a /dev/$d | grep -i "Uncorrectable_Error_Cnt" >> /var/log/Erase-Station/$Dateiname	# S.M.A.R.T.-ID 187
	smartctl -a /dev/$d | grep -i "Command_Timeout" >> /var/log/Erase-Station/$Dateiname			# S.M.A.R.T.-ID 188
	smartctl -a /dev/$d | grep -i "Hardware_ECC_Recovered" >> /var/log/Erase-Station/$Dateiname		# S.M.A.R.T. ID 195
	smartctl -a /dev/$d | grep -i "Reallocated_Event_Count" >> /var/log/Erase-Station/$Dateiname	# S.M.A.R.T. ID 196
	smartctl -a /dev/$d | grep -i "Current_Pending_Sector" >> /var/log/Erase-Station/$Dateiname		# S.M.A.R.T.-ID 197
	smartctl -a /dev/$d | grep -i "Offline_Uncorrectable" >> /var/log/Erase-Station/$Dateiname		# S.M.A.R.T.-ID 198
	smartctl -a /dev/$d | grep -i "Soft_Read_Error_Rate" >> /var/log/Erase-Station/$Dateiname		# S.M.A.R.T.-ID 201
	# Einfuegen einer Leerzeile in die Log-Datei.
	echo "" >> /var/log/Erase-Station/$Dateiname
	# Umrechnung der Variable mit der Laufleistung von Stunden in Jahren.
	years=$(echo "scale=1; $hour/24/365" | bc)
	# Pruefung, ob Laufleistung weniger als 1 Jahr ist.
	if [[ "$years" == .? ]]
		then
			# Falls Laufleistung weniger als 1 Jahr ist, Voranstellen einer fuehrenden Null.
			years=0$years
	fi
	# Laufleistung in Jahren in Prueffbericht einfuegen.
	echo "Power on years: $years" >> /var/log/Erase-Station/$Dateiname
	# Einfuegen einer Leerzeile in die Log-Datei.
	echo "" >> /var/log/Erase-Station/$Dateiname
	# Abfragen gespeicherten Selbsttests aus der Medium-Datenbank.
	smartctl -l selftest /dev/$d >> /var/log/Erase-Station/$Dateiname
	# Datei des erstellten Pruefberichtes in Unterordner /var/log/Erase-Station/Archiv/ verschieben.
	mv /var/log/Erase-Station/$Dateiname /var/log/Erase-Station/Archiv/$Dateiname
	# Dateiinhalt des Pruefberichtes ausgeben und als E-Mail eigens zur erstellten E-Mail-Adresse an Mitarbeiter weiterleiten.
	cat /var/log/Erase-Station/Archiv/$Dateiname | mail -s "[$DT] Security-Erase finished - $Seriennummer" Erase-Station@accelerated.de
	# Statusausgabe, dass Prueffbericht archiviert und an unternehmensinternen E-Mail-Verteiler weiter geleitet wurde.
	DT=$(date +%Y-%m-%d_%H:%M:%S)
	echo "$DT /dev/$d: Report saved in /var/log/Erase-Station/Archiv/$Dateiname and send as mail." >> /dev/tty1
	echo "$DT /dev/$d: Report saved in /var/log/Erase-Station/Archiv/$Dateiname and send as mail." >> /var/log/Erase-Station/log
}

# Methode zum Umgang des Offline-Selbsttests bei Festplaten des Herstellers Hitachi/HGST
SelftestHitachi() {
	# Starten des kurzen Selbsttests und die Standardausgabe in eine Datei als Puffer speichern.
	smartctl -t short /dev/$d > /var/lock/selftest_$d.info
	# Statusausgabe, dass kurzer Offline-Selbsttest ausgefuehrt wurde.
	DT=$(date +%Y-%m-%d_%H:%M:%S)
	echo "$DT /dev/$d: Short selftest started" >> /dev/tty1
	echo "$DT /dev/$d: Short selftest started" >> /var/log/Erase-Station/log
	# Extrahieren der Zeile mit dem String "minutes for test to complete" aus der Puffer-Datei und .
	duration=$(cat /var/lock/selftest_$d.info | grep -i "minutes for test to complete")
	# Deklaration der Variable 'minutes' fuer den numerischen Wert in Minuten aus der Variable 'duration'.
	minutes=$(echo $duration | cut -d" " -f3)
	# Numerischen Wert der Variable 'minutes' in Sekunden umrechnen.
	let seconds=$minutes*60
	# Wartezeit in Sekunden, bis Selbsttest abgschlossen ist.
	sleep $seconds
	# Erstellte Puffer-Datei wieder loeschen.
	rm /var/lock/selftest_$d.info
	# Statusausgabe, dass Selbsttest abgeschlossen ist.
	DT=$(date +%Y-%m-%d_%H:%M:%S)
	echo "$DT dev/$d: Short selftest complete" >> /dev/tty1
	echo "$DT dev/$d: Short selftest complete" >> /var/log/Erase-Station/log
}

# Methode zur Pruefung, ob Offline-Selbsttest abgschlossen ist.
Prozess() {
	while true
	do
		# Deklaration Variable 'Prozess', um aus der Datenbank des Mediums zu Pruefen,
		# ob ein Selbstest noch nicht abgeschlossen wurde.
		Prozess=$(smartctl -l selftest /dev/$d | grep -i progress)
		if [[ "$Prozess" == "" ]]
		# Ist die Variable leer,
		then
			# ... dann Abbruch der Methode,
			break
		else
			# ... ansonsten Wartezeit von 60 Sekunden, und erneute Pruefung.
			sleep 60
		fi
	done
}

# Methode zum Pruefen, ob aktiviertes Medium die Systemplatte mit
# der Laufwerkskennung /dev/sda ist.
if [ $d = "sda" ]
	# Entspricht der Uebergabeparameter der Systemplatte,
	then
		# ... Statusausgabe, dass es sich um die Systemplatte handelt,
		echo "This is the system drive. This can not be erased." >> /dev/tty1
		# ... und das Skript abgbrechen.
		exit 0
fi

# Uebepruefung, dass der String aus dem Uebergabeparameter nicht leer ist.
while [ ! -e /dev/$d ]
do
	sleep 10
done

# Statusausgabe auf dem 1. seriellen Terminal, dass ein Meidum gefunden wurde.
DT=$(date +%Y-%m-%d_%H:%M:%S)
echo "$DT Device found as /dev/$d" >> /dev/tty1
echo "$DT Device found as /dev/$d" >> /var/log/Erase-Station/log
# Leere Zeile auf dem 1. seriellen Terminal ausgeben.
echo >> /dev/tty1
# Die ersten 16 Zeilen mit den Stammdaten des aktivierten Mediumauf dem 1.
# seriellen Terminal ausgeben.
hdparm -I /dev/$d | head -n 16 >> /dev/tty1

frozen=$(hdparm -I /dev/$d | grep -i "not frozen")
# Uebepruefung, ob die Sicherheitseinstellung des Mediums gesperrt sind.
if [ "$frozen" == "not	frozen" ]
	then
		echo "/dev/$d: Reattach the device!" >> /dev/tty1
		exit 0
fi

# Deklaration der Variable 'Password' und Zuweisung dieser mit dem String 'GEHEIM'.
Password=GEHEIM
# Die Sicherheitseinstellungen des Mediums mit dem Passwort 'Password' aktivieren.
hdparm --user-master u --security-set-pass $Password /dev/$d
# Statusausgabe, dass die Sicherheitseinstellungen auf Medium gesetzt wurden.
DT=$(date +%Y-%m-%d_%H:%M:%S)
echo "§DT /dev/$d: Password set" >> /dev/tty1
echo "$DT /dev/$d: Password set" >> /var/log/Erase-Station/log
# Deklaration der Variable 'EnhancedErase' und aus den Sicherheitsinformationen
# die Verfuegbarkeit des Erweiterten Loeschmodus extrahieren.
EnhancedErase=$(hdparm -I /dev/$d | grep -i "enhanced erase" | cut -f2)
# Ist der Erweiterte Loeschmodus nicht vorhanden, ...
if [ "$EnhancedErase" == "not" ]
	then
		# ... den einfachen, veralteten Loeschmodus auf dem Medium anwenden.
		# Mit Statusausgabe des Beginns.
		DT=$(date +%Y-%m-%d_%H:%M:%S)
		echo "$DT /dev/$d: Start Security-Erace" >> /dev/$d
		echo "$DT /dev/$d: Start Security-Erace" >> /var/log/Erase-Station/log
		hdparm --user-master u --security-erase $Password /dev/$d
		# Statusausgabe, das der Loeschvorgang beendet ist.
		DT=$(date +%Y-%m-%d_%H:%M:%S)
		echo "$DT /dev/$d: Security-Erase finished" >> /dev/tty1
		echo "$DT /dev/$d: Security-Erase finished" >> /var/log/Erase-Station/log
	else
		# ... den erweiterten Loeschmodus auf dem Medium anwenden.
		# Mit Statusausgabe des Beginns.
		DT=$(date +%Y-%m-%d_%H:%M:%S)
		echo "$DT /dev/$d: Start Security-Erase-Enhanced" >> /dev/$d
		echo "$DT /dev/$d: Start Security-Erase-Enhanced" >> /var/log/Erase-Station/log
		hdparm --user-master u --security-erase-enhanced $Password /dev/$d
		# Statusausgabe, das der Loeschvorgang beendet ist.
		DT=$(date +%Y-%m-%d_%H:%M:%S)
		echo "$DT /dev/$d: Security-Erase-Enhanced finished" >> /dev/tty1
		echo "$DT /dev/$d: Security-Erase-Enhanced finished" >> /var/log/Erase-Station/log
fi

# Der Variable 'VendorEntry' die Zeile mit Herstellernamen aus den Stammdaten des Mediums deklarieren.
VendorEntry=$(smartctl -a /dev/$d | grep -i "model family")
# Der Variable 'Vendor' den Namen aus der Variable 'VendorEntry' extrahieren.
Vendor=$(echo $VendorEntry | cut -d" " -f3)

# Fallpruefung, ueber den Hersteller des Mediums.
case "$Vendor" in
	# Beim Hersteller Hitachi die Methode 'SelftestHitachi' zum Offline-Selbsttest ausfuehren.
	Hitachi)SelftestHitachi
		;;
	# Beim Hersteller "Hitachi Global Storage" die Methode 'SelftestHitachi' zum Offline-Selbsttest ausfuehren.
	HGST)	SelftestHitachi
		;;
	# Bei allen restlichen Herstellern:
	# Ausgabe der in der Datenbank des Mediums die Liste aller ducrhgefuehrten 
	# Selbsttests in die Puffer-Datei /var/lock/smartctl0_$d.lock schreiben.
	*)	smartctl -l selftest /dev/$d | grep offline > /var/lock/smartctl0_$d.lock
		# Aus der erzeugten Puffer-Datei die ersten 5 zeile entfernen und den Rest in
		# eine weitere Puffer-Datei schreiben.
		sed '6,$d' /var/lock/smartctl0_$d.lock > /var/lock/smartctl1_$d.lock
		# Die zweite Puffer-Datei in den Dateinamen der ersten umbenennen.
		mv /var/lock/smartctl1_$d.lock /var/lock/smartctl0_$d.lock
		# Den Inhalt der zurueck gebliebenen Datei nach der Zeichenkette 'extended'
		# und erneut in eine weitere Puffer-Datei schreiben.
		cat /var/lock/smartctl0_$d.lock | grep -i extended > /var/lock/smartctl1_$d.lock
		# Der Variable 'TestEntry' den Inhalt der wieder erneuten Puffer-Datei zuweisen.
		TestEntry="$(cat /var/lock/smartctl1_$d.lock)"
		# Deklaration der Variable 'Completed' die Zeichenkette zwischen
		# 5. und 8. Leerzeichen zuweisen.
		Completed=$(echo $TestEntry | cut -d" " -f5-7)
		# Beide Puffer-Dateien wieder loeschen.
		rm /var/lock/smartctl{0,1}_$d.lock
		# Uebepruefung, ob die Variable 'Completed' den String "Completed without error"
		# beinhaltet.:
		if [ "$Completed" == "Completed without error" ]
			then
				# Wenn Ja: kurzen Offline-Selbsttest durchfuehren.
				smartctl -t short /dev/$d
				#30 Sekunden Wartezeit, bis Befehl ausgefuehrt wurde.
				sleep 30
				# Statusausgabe, das der Test gestartet wurde.
				DT=$(date +%Y-%m-%d_%H:%M:%S)
				echo "$DT /dev/$d: Short selftest started" >> /dev/tty1
				echo "$DT /dev/$d: Short selftest started" >> /var/log/Erase-Station/log
				# Aufrufen der Methode 'Prozess', um zu kontrollieren, bis Test vorbei ist.
				Prozess
				# Statusausgabe, das der Test beendet wurde.
				DT=$(date +%Y-%m-%d_%H:%M:%S)
				echo "$DT /dev/$d: Short selftest complete" >> /dev/tty1
				echo "$DT /dev/$d: Short selftest complete" >> /var/log/Erase-Station/log
			else
				# Wenn Nein: langen Offline-Selbsttest durchfuehren.
				smartctl -t long /dev/$d
				#30 Sekunden Wartezeit, bis Befehl ausgefuehrt wurde.
				sleep 30
				# Statusausgabe, das der Test gestartet wurde.
				DT=$(date +%Y-%m-%d_%H:%M:%S)
				echo "$DT /dev/$d: Long selftest started" >> /dev/tty1
				echo "$DT /dev/$d: Long selftest started" >> /var/log/Erase-Station/log
				# Aufrufen der Methode 'Prozess', um zu kontrollieren, bis Test vorbei ist.
				Prozess
				# Statusausgabe, das der Test beendet wurde.
				DT=$(date +%Y-%m-%d_%H:%M:%S)
				echo "$DT /dev/$d: Long selftest complete" >> /dev/tty1
				echo "$DT /dev/$d: Long selftest complete" >> /var/log/Erase-Station/log
		fi
		;;
esac

# Ausfuehren der Methode zum Erstellen des Pruefberichtes.
Report
# Statusausgabe, dass der Loesch-, Pruef- und Analysevorgang des Mediums abgeschlossen ist.
DT=$(date +%Y-%m-%d_%H:%M:%S)
echo "$DT /dev/$d: Secure-Erase finished" >> /dev/tty1
echo "$DT /dev/$d: Secure-Erase finished" >> /var/log/Erase-Station/log