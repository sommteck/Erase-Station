#!/bin/bash
#
# Dies ist das Shell-Skript zur Installation und Einrichtung der nötigen Software-
# komponenten für die Löschstation.

apt-get unstall -y lshw lsusb postfix libsasl2-modules bsd-mailx lsscsi lsblk cfdisk truncate
cp Erase-Station.rule /lib/udev/rules.d/Erase-Station.rule
cp Deactivate-USB-Suspend.rule /lib/udev/rules.d/Deactivate-USB-Suspend.rule
mkdir /root/Skripte_Erase-Station/
cp detect_disk.sh /root/Skripte_Erase-Station/detect_disk.sh
cp erase_disk.sh /root/Skripte_Erase-Station/erase_disk.sh
chmod 0740 /root/Skripte_Erase-Station/*_disk.sh
mkdir /var/log/Erase-Station/
mkdir /var/log/Erase-Station/Archiv/
chmod –R 0664 /var/log/Erase-Station/
