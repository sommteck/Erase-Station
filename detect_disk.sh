#!/bin/bash
#
# Dies ist das Shellskript detect_disk.sh. Es dient im Rahmen des Projektes zum Prototyping
# einer Loeschstation zum Erkennen, ob eine Festplatte oder SSD mit dem Host aktiviert wurde.
# Initiiert wird dies Skript durch die udev-Regel <Dateiname> unter <Dateipfad>!!!!


# Mit dem Start werden die letzten 60 Zeilen der Datei /var/log/syslog nach der Zeichenkette
# "attached scsi disk" durchsucht und in eine Puffer-Datei geschrieben geschrieben.
tail -n 60 /var/log/syslog | grep -i "attached scsi disk" > /var/lock/buffer0.lock
# Der Inhalt der Puffer-Datei wird bis auf die letzte Zeile entfernt und die verbleibende Zeile
# in eine weitere Puffer-Datei gespeichert.
cat /var/lock/buffer0.lock | tail -n 1 > /var/lock/buffer1.lock
# Deklaration und Zuweisung der Variable 'device' mit der heraus geloesten Laufwerkskennung.
device=$(cat /var/lock/buffer1.lock | cut -d[ -f3 | cut -d] -f1)
# Beide Puffer-Dateien werden wieder gelöscht.
rm /var/lock/buffer*
# Es wird ueberprueft, ob es bereits einen Prozess gibt, welcher diese Shellskript mit der selben
# Laufwerkskennung ausfuehrt. Das Ergebnis wird der deklarierten Variable 'Process' zugewiessen.
var3=$(ps -ax | grep "erase_disk.sh" | grep -v "grep")
Process=$(echo $var3 | cut -d" " -f7)
# Ueberpruefung, ob eine Uebereinstimmung stattfindet.
if [ $device = $Process ]
	then
		# Wenn Ja: Wird das Skript abgebrochen
		echo "Program is now in process!"
		exit 0
	else
		# Wenn Nein: Wird Shellskript zum Analysieren, Loeschen und Reporting mit der Laufwerks-
		# kennung als Uebergabeparameter initiiert.
		/bin/bash /root/Skripte_Erase-Station/erase_disk.sh $device
fi